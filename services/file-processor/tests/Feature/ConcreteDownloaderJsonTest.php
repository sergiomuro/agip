<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConcreteDownloaderJsonTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);

       /* $this->visit('/')
            ->see('Laravel 5');*/
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testJsonCreate()
    {
        $limit = 10;
        $response = $this->get('/json?limit=' . $limit);
        $response
            ->assertStatus(200);
    }
}
