<?php

namespace App\Http\Controllers\Downloader;
ini_set('error_log', '/tmp/php.log');

use App\Http\Controllers\Downloader\Entities\File;
use Exception;
use Illuminate\Support\Facades\Log;
use ZipArchive;

abstract class AbstractDownloaderController
{
    const LOG_TYPE_ITALIC = -1;
    const LOG_TYPE_DEBUG = 0;
    const LOG_TYPE_INFO = 1;
    const LOG_TYPE_ERROR = 2;

    public $fileName = null;

    abstract public function getResourceFile(?string $isLocalFile): string;

    abstract public function getProcessFile(string $folderName): string;

    public function __construct()
    {
        $dirname = $this->getStoragePath();

        if (!is_dir($dirname)) {
            $folderPermissions = (env('APP_ENV') == 'local' ? 0777 : 0755);
            mkdir($dirname, $folderPermissions, true);
        }
    }

    public function getStoragePath(): string
    {
        return public_path() . '/tmp/';
    }

    public function downloadFile(?string $isLocalFile): string
    {
        return $this->getResourceFile($isLocalFile);
    }

    public function processDownloaded(string $folderName): string
    {
        return $this->getProcessFile($folderName);
    }

    public function unzipFIle(string $filePath, string $fileName): string
    {
        $filePathInfo = pathinfo($fileName);
        if (empty($filePathInfo['extension'])) {
            throw new Exception('File without extension.', 500);
        }

        $extractFolder = $this->getStoragePath() . $filePathInfo['filename'];
        if (is_file($extractFolder)) {
            return $extractFolder;
        }

        if ($filePathInfo['extension'] === 'zip') {
            $zip = new ZipArchive;
            $res = $zip->open($filePath . $fileName);
            if ($res === TRUE) {
                $zip->extractTo($extractFolder);
                $zip->close();
                return $extractFolder;
            } else {
                throw new Exception('Can\'t extract file', 500);
            }
        }

        return null;
    }

    /**
     * Read files from folder
     *
     * @param string $folderName
     * @return array
     */
    public function readFilesIntoFolder(string $folderName): array
    {
        self::log('Reading files into ' . $folderName);
        $files = scandir($folderName, 1);
        $remove = ['.', '..'];
        $result = array_diff($files, $remove);
        return $result;
    }

    /**
     * @return string|null
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return void
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * Log into docker console and logs
     *
     * @param string|null $message
     * @param int $errorType
     */
    public static function log(string $message, int $errorType = self::LOG_TYPE_DEBUG): void
    {
        $colorFormats = array(
            // styles
            // italic and blink may not work depending of your terminal
            'bold' => "\033[1m%s\033[0m",
            'dark' => "\033[2m%s\033[0m",
            'italic' => "\033[3m%s\033[0m",
            'underline' => "\033[4m%s\033[0m",
            'blink' => "\033[5m%s\033[0m",
            'reverse' => "\033[7m%s\033[0m",
            'concealed' => "\033[8m%s\033[0m",
            // foreground colors
            'black' => "\033[30m%s\033[0m",
            'red' => "\033[31m%s\033[0m",
            'green' => "\033[32m%s\033[0m",
            'yellow' => "\033[33m%s\033[0m",
            'blue' => "\033[34m%s\033[0m",
            'magenta' => "\033[35m%s\033[0m",
            'cyan' => "\033[36m%s\033[0m",
            'white' => "\033[37m%s\033[0m",
            // background colors
            'bg_black' => "\033[40m%s\033[0m",
            'bg_red' => "\033[41m%s\033[0m",
            'bg_green' => "\033[42m%s\033[0m",
            'bg_yellow' => "\033[43m%s\033[0m",
            'bg_blue' => "\033[44m%s\033[0m",
            'bg_magenta' => "\033[45m%s\033[0m",
            'bg_cyan' => "\033[46m%s\033[0m",
            'bg_white' => "\033[47m%s\033[0m",
        );

        switch ($errorType) {
            default:
            case self::LOG_TYPE_DEBUG:
                Log::debug($message);
                $color = 'green';
                break;
            case self::LOG_TYPE_INFO:
                Log::info($message);
                $color = 'cyan';
                break;
            case self::LOG_TYPE_ERROR:
                Log::error($message);
                $color = 'red';
                break;
            case self::LOG_TYPE_ITALIC:
                $color = 'italic';
                break;
        }

        $message = sprintf($colorFormats[$color], $message);
        // 4 message is sent directly to the SAPI logging handler.
        error_log($message, 4);
    }
}
