<?php

namespace App\Http\Controllers\Downloader;

use App\Http\Controllers\Downloader\Entities\File;
use Exception;
use ZipArchive;

abstract class AbstractDownloaderController
{
    public $fileUri = null;

    abstract public function getResourceFile(): string;

    abstract public function getProcessFile(string $folderName): string;

    public function __construct()
    {
        $dirname = $this->getStoragePath();

        if (!is_dir($dirname)) {
            mkdir($dirname, 0755, true);
        }
    }

    public function getStoragePath(): string
    {
        return public_path() . '/tmp/';
    }

    public function downloadFile(): string
    {
        return $this->getResourceFile();
    }

    public function processDownloaded(string $folderName): string
    {
        return $this->getProcessFile($folderName);
    }

    public function unzipFIle(string $filePath, string $fileName) : string
    {
        $filePathInfo = pathinfo($fileName);
        if (empty($filePathInfo['extension'])) {
            throw new Exception('File without extension.', 500);
        }

        $extractFolder = $this->getStoragePath() . $filePathInfo['filename'];
        if (is_file($extractFolder)) {
            return $extractFolder;
        }

        if ($filePathInfo['extension'] === 'zip') {
            $zip = new ZipArchive;
            $res = $zip->open($filePath . $fileName);
            if ($res === TRUE) {
                $zip->extractTo($extractFolder);
                $zip->close();
                return $extractFolder;
            } else {
                throw new Exception('Can\'t extract file' , 500);;
            }
        }

        return null;
    }

    public function readFilesIntoFolder(string $folderName) : array
    {
        $files = scandir($folderName, 1);
        $remove = [',', '..'];
        $result = array_diff($files, $remove);
        return $result;
    }
}
