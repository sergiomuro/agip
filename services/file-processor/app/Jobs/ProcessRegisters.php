<?php

namespace App\Jobs;

use App\Http\Controllers\Downloader\Entities\File;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessRegisters implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $registers;

    /**
     * Create a new job instance.
     *
     * @param array $registers
     */
    public function __construct(array $registers)
    {
        $this->registers = $registers;
    }

    /**
     * Execute the job.
     *
     * @param File $processor
     * @return void
     */
    public function handle(File $processor)
    {
        dd($processor);

        // Process uploaded podcast...
    }
}
