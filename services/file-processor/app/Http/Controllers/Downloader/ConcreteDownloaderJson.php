<?php

namespace App\Http\Controllers\Downloader;

use App\Http\Controllers\Downloader\Entities\File;
use App\Http\Controllers\Downloader\Entities\JsonFile;
use App\Http\Controllers\Downloader\Entities\LocalFile;
use App\Jobs\ProcessRegisters;
use Illuminate\Support\Facades\Log;
use Exception;
use SebastianBergmann\CodeCoverage\Report\PHP;

class ConcreteDownloaderJson extends AbstractDownloaderController
{
    const FILE_URL = 'http://ofiya.net/newsletter/';
    const LOCAL_PATH = '/tmp-example/';
    public $client = null;

    /**
     * Limit only for development
     * @var int
     */
    private $devLimit = 0; // 0 Zero unlimited

    const ID_PERSON_LENGTH = 11;
    const UPDATE_LENGTH = 16;
    const END_CODE = 999999;
    const LINE_LENGTH = 380; // Auto calculated into readMaxLengthLine()

    const ID_PERSONAL_DATA = '01',
        ID_ACTIVITIES_DATA = '02',
        ID_TAXES_DATA = '03',
        ID_TAXES_CATEGORY_DATA = '04',
        ID_ADDRESSES_DATA = '05',
        ID_PHONES_DATA = '06',
        ID_EMAILS_DATA = '07';

    const RETURN_TYPE_STRING = 'string',
        RETURN_TYPE_INTEGER = 'integer';

    /**
     * Mapping const MAP_MAXLENGTH_LINES
     * keyName => [maxlength, type (Default: String)]
     */
    const MAP_KEY_LINES = [
        'actividadList' => self::ID_ACTIVITIES_DATA,
        'impuestoList' => self::ID_TAXES_DATA,
        'categoriaList' => self::ID_TAXES_CATEGORY_DATA,
        'domicilioList' => self::ID_ADDRESSES_DATA,
        'telefonoList' => self::ID_PHONES_DATA,
        'emailList' => self::ID_EMAILS_DATA,
    ];

    /**
     * Mapping with data for parsing
     * keyName => [maxlength, type (Default: String)]
     */
    const MAP_MAXLENGTH_LINES = [
        'actividadList' => [
            'idActividad' => ['maxlength' => 6, 'type' => self::RETURN_TYPE_INTEGER],
            'orden' => ['maxlength' => 3, 'type' => self::RETURN_TYPE_INTEGER],
            'estado' => ['maxlength' => 2],
            'periodo' => ['maxlength' => 6, 'type' => self::RETURN_TYPE_INTEGER],
            'esVigente' => ['maxlength' => 1],
            'codNomenclador' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
        ],
        'impuestoList' => [
            'idImpuesto' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'periodo' => ['maxlength' => 6, 'type' => self::RETURN_TYPE_INTEGER],
            'estado' => ['maxlength' => 2],
            'esVigente' => ['maxlength' => 1],
            'idMmotivo' => ['maxlength' => 6, 'type' => self::RETURN_TYPE_INTEGER],
            'idMunicipio' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            // periodoDesde cambia a 8 por definicion de documento AAAAMMDD
            'periodoDesde' => ['maxlength' => 8, 'type' => self::RETURN_TYPE_INTEGER],
        ],
        'categoriaList' => [
            'idImpuesto' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'idCategoria' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'periodo' => ['maxlength' => 6],
            'esVigente' => ['maxlength' => 1],
            'estado' => ['maxlength' => 2],
        ],
        'domicilioList' => [
            'idTipoDomicilio' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'orden' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'idEstadoDomicilio' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'codigoPostal' => ['maxlength' => 8],
            'localidad' => ['maxlength' => 60],
            'idProvincia' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'idNomenclador' => ['maxlength' => 9, 'type' => self::RETURN_TYPE_INTEGER],
            'datoAdicional' => ['maxlength' => 200],
            'idTipoDatoAdicDomicilio' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'direccion' => ['maxlength' => 60],
            'idDestinoComercial' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
        ],
        'telefonoList' => [
            'idPais' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'area' => ['maxlength' => 4, 'type' => self::RETURN_TYPE_INTEGER],
            'numero' => ['maxlength' => 15, 'type' => self::RETURN_TYPE_INTEGER],
            'idTipoTelefono' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'idTipoLinea' => ['maxlength' => 3, 'type' => self::RETURN_TYPE_INTEGER],
        ],
        'emailList' => [
            'direccion' => ['maxlength' => 60],
            'idTipoEmail' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
            'idEstadoEmail' => ['maxlength' => 2, 'type' => self::RETURN_TYPE_INTEGER],
        ],

    ];

    private $processedRegisters = 0;
    private $fileOpen = null;

    /**
     * ConcreteDownloaderJson constructor.
     * @param String $fileName
     * @param int|null $devLimit
     */
    public function __construct(String $fileName, ?int $devLimit)
    {
        parent::__construct();

        $this->setFileName($fileName);

        if (!empty($devLimit)) {
            $this->devLimit = $devLimit;
        }
    }

    /**
     * Get Max Length of data array
     * It used to make all lines with the same length
     *
     * @return int
     */
    private function readMaxLengthLine(): int
    {
        $length = 0;
        foreach (self::MAP_MAXLENGTH_LINES as $k => $array) {
            $sum = self::ID_PERSON_LENGTH +
                self::UPDATE_LENGTH +
                strlen(self::MAP_KEY_LINES[$k]);
            foreach ($array as $kk => $data) {
                $sum += $data['maxlength'];
            }
            if ($length < $sum) {
                $length = $sum;
            }
        }

        return $length;
    }

    /**
     * Make a opened file to insert the data
     *
     * @param string $fileName
     */
    private function createOpenFile(string $fileName)
    {
        $this->fileOpen = fopen($this->getStoragePath() . $fileName . '_final.txt', 'w');
    }

    /**
     * Read file and unzip
     *
     * @param string|null $isLocalFile
     * @return string
     * @throws Exception
     */
    public function getResourceFile(?string $isLocalFile): string
    {
        self::log('Getting file: ' . public_path() . self::LOCAL_PATH . $this->getFileName());
        $this->client = new JsonFile(public_path() . self::LOCAL_PATH, $this->getFileName(), $this->getStoragePath());
        if ($this->client->getFile($isLocalFile)) {
            self::log('Ready file: ' . $this->getStoragePath() . $this->getFileName());
            self::log('Unizipping file: ' . $this->getStoragePath() . $this->getFileName());
            return $this->unzipFIle($this->getStoragePath(), $this->getFileName());
        }

        return null;
    }

    /**
     * Convert data of files to array
     *
     * @param string $folderName
     * @return array
     */
    private function convertDataToArray(string $folderName): array
    {
        set_time_limit(0);
        $newData = null;
        $files = $this->readFilesIntoFolder($folderName);
        self::log('Files found (' . count($files) . ') into ' . $folderName);

        foreach ($files as $key => $fileName) {
            self::log('Converting JSON data to array from ' . $fileName);
            $chunk = '';
            $filePath = $folderName . '/' . $fileName;
            $fn = fopen($filePath, "r");

            $i = 0;
            while (!feof($fn)) {
                $i++;
                $result = fgets($fn);
                $chunk .= $result;
            }
            fclose($fn);

            $newData[$fileName] = json_decode($chunk, true);
        }
        return $newData;
    }

    /**
     * Add spaces to string
     *
     * @param string $string
     * @param int $maxLength
     * @param string|null $extraData
     * @param string $returnType
     * @return string
     * @throws Exception
     */
    private function addSpacesString(string $string, int $maxLength, string $extraData = null, string $returnType = self::RETURN_TYPE_STRING): string
    {
        // Fix issue spaces with find "ñ"
        //$totalEnies = substr_count(strtoupper($string), "[Ñ|\u00BA|º]");
       /* preg_match_all("/Ñ|\u00BA|º|°/i", $string,$totalEnies);
        $totalEnies = count(array_filter($totalEnies[0]));
        if ($totalEnies > 0) {
            $maxLength += $totalEnies;
        }*/

        if (strlen($string) > $maxLength) {
            self::log("Max length exceed: " . $string . '. Process stopped.', self::LOG_TYPE_ERROR);
            throw new Exception("Max length exceed", 500);
        }

        $strPadChar = '0';
        $strPadPos = STR_PAD_LEFT;
        if ($returnType === self::RETURN_TYPE_STRING) {
            $strPadChar = ' ';
            $strPadPos = STR_PAD_RIGHT;
        }

        return str_pad(($string ?? ''), $maxLength, $strPadChar, $strPadPos) . $extraData;
    }

    private function addSpaces(array $array, string $key, int $maxLength, string $extraData = null, string $returnType = self::RETURN_TYPE_STRING): string
    {
        $string = ($array[$key] ?? '');
        return $this->addSpacesString($string, $maxLength, $extraData, $returnType);
    }

    /**
     * Write lines into file
     *
     * @param $string
     * @param bool $EOL
     */
    private function writeLine($string, bool $EOL = true)
    {
        $breakLine = null;

        if ($EOL) {
            $breakLine = PHP_EOL;
        }

        fwrite($this->fileOpen, $string . $breakLine);
    }

    /**
     * Check if array is multidimensional
     *
     * @param $array
     * @return bool
     */
    function isMultiArray($array)
    {
        if (count($array) == count($array, COUNT_RECURSIVE)) {
            return false;
        } else {
            return true;
        }
    }

    private $textDocument = null;

    /**
     * Recursive implode array to string
     *
     * @param $array
     * @throws Exception
     */
    function multi_implode($array)
    {
        $maxLength = ($this->readMaxLengthLine());

        foreach ($array as $k => $personData) {
            if (is_array($personData)) {
                if ($this->isMultiArray($personData)) {
                    $this->multi_implode($personData);
                    continue;
                }
                //$this->textDocument .= join($personData) . PHP_EOL;
                //$personData = $this->addSpacesString(join($personData), self::LINE_LENGTH);
                $personData = $this->addSpacesString(join($personData), $maxLength);
                $this->writeLine($personData);
                continue;
            }
            //$this->textDocument .= $personData . PHP_EOL;
            $this->writeLine($personData);
        }
    }

    private function recursiveTextDocument($array): ?string
    {
        foreach ($array as $person) {
            try {
                $this->multi_implode($person);
            } catch (Exception $e) {
                self::log($e->getMessage(), self::LOG_TYPE_ERROR);
            }
        }

        return $this->textDocument;
    }

    public function secondsToTime($s)
    {
        $h = floor($s / 3600);
        $s -= $h * 3600;
        $m = floor($s / 60);
        $s -= $m * 60;
        return $h . ':' . sprintf('%02d', $m) . ':' . sprintf('%02d', $s);
    }

    public function backLine()
    {
        //   ESC \X1B    [          1         F
        $back = chr(27) . chr(91) . chr(49) . 'F';
        // Erase to the end of the line
        //$back .= "\033[K";

        return $back;
    }


    /**
     * Process files into folder
     *
     * @param string $folderName
     * @return string
     * @throws Exception
     */
    public function getProcessFile(string $folderName): string
    {
        $primaryKey = 'novedades';
        self::log("Start Process", self::LOG_TYPE_INFO);
        $files = $this->convertDataToArray($folderName);
        foreach ($files as $fileName => $data) {
            if ($this->devLimit) {
                $data[$primaryKey] = array_slice($data[$primaryKey], 0, $this->devLimit);
            }
            $totalRecords = count($data[$primaryKey]);
            self::log('Processing ' . $totalRecords . ' records from ' . $fileName);
            $array = [];
            $fileRegisters = 0;
            $timeEnd = 0;
            $timeStart = microtime(true);
            $this->createOpenFile($fileName);
            foreach ($data[$primaryKey] as $i => $line) {
                $current = $i + 1;
                $userId = $line['id'];

                $dataArray = [
                    [
                        $this->addSpacesString($userId, self::ID_PERSON_LENGTH, self::ID_PERSONAL_DATA),
                        $this->addSpaces($line['clave'] ?? [], 'fechaActualizacion', 16),
                        $this->addSpaces($line, 'tipo_persona', 1),
                        $this->addSpaces($line, 'sexo', 1),
                        $this->addSpaces($line, 'estadoId', 1),
                        $this->addSpaces($line, 'fechaNacimiento', 16),
                        $this->addSpaces($line, 'fechaFallecimiento', 16),
                        $this->addSpaces($line, 'idTipoDocumento', 2, null, self::RETURN_TYPE_INTEGER),
                        $this->addSpaces($line, 'documento', 20, null, self::RETURN_TYPE_INTEGER),
                        $this->addSpaces($line, 'nombre', 50),
                        $this->addSpaces($line, 'apellido', 50),
                        $this->addSpaces($line, 'idactivo', 11, null, self::RETURN_TYPE_INTEGER),
                    ],
                ];

                foreach (self::MAP_MAXLENGTH_LINES as $k => $mapArrayData) {
                    if (empty($line[$k])) {
                        continue;
                    }

                    foreach ($line[$k] as $data) {
                        $arrayDataMapped = [];
                        $arrayDataMapped[] = $this->addSpacesString($userId, self::ID_PERSON_LENGTH, self::MAP_KEY_LINES[$k]);
                        $arrayDataMapped[] = $this->addSpaces($line['clave'] ?? [], 'fechaActualizacion', self::UPDATE_LENGTH);
                        foreach ($mapArrayData as $keyMap => $mapArrayData2) {
                            $arrayDataMapped[] = $this->addSpaces(
                                $data,
                                $keyMap,
                                $mapArrayData2['maxlength'],
                                null,
                                $mapArrayData2['type'] ?? self::RETURN_TYPE_STRING
                            );
                        }
                        $dataArray[] = $arrayDataMapped;
                    }

                }

                $dataArray = array_filter($dataArray);

                $array[$line['id']] = $dataArray;

                $this->recursiveTextDocument([$dataArray]);
                $fileRegisters += count($dataArray);
                /*if ($this->devLimit) {
                    if ($i > $this->devLimit) {
                        break;
                    }
                }*/


                /***
                 * SHOW TIME STATICS
                 */
                $percentage = round(($current * 100) / $totalRecords);
                if (!$timeEnd && $percentage == 1) {
                    $timeEnd = microtime(true) - $timeStart;
                }

                $time = round($timeEnd * ($totalRecords - $current));
                $timeAverage = $this->secondsToTime(($time));
                $timeStart = microtime(true);

                $return = $this->backLine();
                if ($current == $totalRecords) {
                    $return = null;
                }

                self::log('Processing ' . $current . '/' . $totalRecords . ' records. ' . $percentage . '% Completed. Est. Time Left: ' . $timeAverage . $return, self::LOG_TYPE_ITALIC);

            }

            $endLine = self::END_CODE . $this->addSpacesString($fileRegisters, 7, null, self::RETURN_TYPE_INTEGER);
            self::log("Records Inserted: " . $fileRegisters);
            $this->writeLine($endLine, false);
            fclose($this->fileOpen);

            $this->processedRegisters += $fileRegisters;
        }

        self::log("Total Records Inserted: " . $this->processedRegisters, self::LOG_TYPE_INFO);
        self::log("End Process.", self::LOG_TYPE_INFO);

        return response()->json([
                'limit' => ($this->devLimit ? $this->devLimit : 'unlimited'),
                'total_files' => count($files),
                'total_registers_proceeded' => $this->processedRegisters,
            ], 200)
            // prevent show json headers
            ->getContent();
    }
}
