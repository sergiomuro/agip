<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/json', 'Downloader\DownloaderController@json');

/*
Route::get('/redis', function () {
    $queue = Queue::push('LogMessage',array('message'=>'Time: '.time()));
    return $queue;
});
class LogMessage{
    public function fire($job, $date){
        File::append(app_path().'/queue.txt',$date['message'].PHP_EOL);
        $job->delete();
    }
}*/
