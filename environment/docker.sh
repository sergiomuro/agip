# Environment file
AGIP_ENV_FILE=./docker/.env

if [ -f $AGIP_ENV_FILE ] ; then

  source $AGIP_ENV_FILE

  export $(cut -d= -f1 $AGIP_ENV_FILE)
fi

NORMAL='\033[0m'
RED='\033[1;31m'
BLUE='\e[94m'
YELLOW='\e[93m'

help() {
  echo "-----------------------------------------------------------------------"
  echo "                 EGIP Environment                                     -"
  echo "-----------------------------------------------------------------------"
  echo -e -n "$BLUE"
  echo "   > help                       - Show commands"
  echo "   > build                      - Configure local environment and build images"
  echo "   > up                         - Start containers"
  echo "   > stop                       - Stop containers"
  echo "   > status                     - Show active containers"
  echo "   > remove                     - Stop and remove containers"
  echo "   > prune                      - Docker system prune"
  echo -e -n "$NORMAL"
  echo "-----------------------------------------------------------------------"
}

log() {
  echo -e -n "${YELLOW}$1${NORMAL}\n"
}

error() {
  echo -e -n "${RED}$1${NORMAL}\n"
}

build() {
    enviroment

    stop

    log "Building images..."

    docker-compose -f docker/docker-compose.yml build --pull

    [ $? != 0 ] && \
        error "Docker image build failed ! " && exit 100

    log "Success!"
}

up() {
    log "Start containers"

    docker-compose -f docker/docker-compose.yml up
}

stop() {
    log "Stop containers"

    docker-compose -f docker/docker-compose.yml stop
}

status() {
    log "Up containers"

    docker ps --format '{{.Names}}'
}

remove() {
    log "Remove containers"

    docker stop $(docker ps -a -q)

    docker rm $(docker ps -a -q) --force
}

prune() {
    remove

    log "Docker system prune"

    docker system prune
}

set-vars() {
    if [ -f $AGIP_ENV_FILE ] ; then
        log "Set enviroment vars"

        source $AGIP_ENV_FILE

        export $(cut -d= -f1 $AGIP_ENV_FILE)
    fi
}

enviroment() {
    log "Creating Environment..."

    REWRITE="yes"

    if [ -f $AGIP_ENV_FILE ]; then
        log "${AGIP_ENV_FILE} exist, rewrite?(yes|no)?"

        read REWRITE
    fi

    if [ "$REWRITE" == "no" ]; then
        source $AGIP_ENV_FILE

        export $(cut -d= -f1 $AGIP_ENV_FILE)

        return 0
    fi

    read -e -p "Local workspace path? " -i "${HOME}/workspace/services" AGIP_WORKSPACE

    read -e -p "Docker workspace path? " -i "/services" AGIP_DOCKER_WORKSPACE

    read -e -p "MariaDB root password? " -i "root" MYSQL_ROOT_PASSWORD

    log "Saving Enviroment file ${AGIP_ENV_FILE}"

    touch $AGIP_ENV_FILE

    echo "AGIP_WORKSPACE=${AGIP_WORKSPACE}" > $AGIP_ENV_FILE

    echo "AGIP_DOCKER_WORKSPACE=${AGIP_DOCKER_WORKSPACE}" >> $AGIP_ENV_FILE

    echo "MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}" >> $AGIP_ENV_FILE

    source $AGIP_ENV_FILE

    export $(cut -d= -f1 $AGIP_ENV_FILE)
}

$*