<?php

namespace App\Http\Controllers\Downloader;

use App\Http\Controllers\Controller;

class DownloaderController extends Controller
{
    public function index()
    {
        $client = new ConcreteDownloaderLocal('EV_SR_PADRON_PROVINCIAS_20190517_130643.zip');
        $folder = $client->downloadFile();
        echo $client->processDownloaded($folder);
    }
}
