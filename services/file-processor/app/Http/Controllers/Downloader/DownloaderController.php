<?php

namespace App\Http\Controllers\Downloader;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DownloaderController extends Controller
{
    public function index()
    {
        $client = new ConcreteDownloaderLocal('EV_SR_PADRON_PROVINCIAS_20190517_130643.zip');
        $folder = $client->downloadFile();
        echo $client->processDownloaded($folder);
    }

    public function json(Request $request)
    {
        $fileName = 'padron_mono_prov_json_0_20190917_164044_QA.zip';
        $limit = null;
        if ($request->input('limit')) {
            $limit = $request->input('limit');
        }

        $client = new ConcreteDownloaderJson($fileName, $limit);
        $folder = $client->downloadFile(Config('constants.local_file'));
        echo $client->processDownloaded($folder);
    }
}
