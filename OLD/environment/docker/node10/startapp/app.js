'use strict';

const http = require('http');

const PORT = 8181;
const HOST = '0.0.0.0';

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('okay');
}).listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);