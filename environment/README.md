# AGIP Environment

## Empezando
Las siguientes instrucciones son para tener el ambiente ejecutándose localmente en su maquina, con los servicios:

* PHP 7.3 (MS SQL Server & DB2)
* Nginx

## Prerrequisitos
* svn|git
* Linux(preferentemente Ubuntu >= 14)
* docker >= 17.12
* docker-compose >= 1.19

## Construir e iniciar contenedores
Clonar o copiar `environment` preferentemente en `$HOME/environment`.
Si no posee esta estructura de carpetas puede crearlas o mapear su estructura actual.

### Construir imágenes
Ejecutar desde el directorio donde clono `environment` 
```
./docker.sh build
```
La construcción de las imágenes pueden demorar varios minutos, dependido la conexión a Internet y la velocidad de su máquina.

## Directorio local de los proyectos
Cree un directorio donde se alojaran los proyectos, preferentemente en `$HOME/services` con permiso `755`

### Variables del entorno
Tenga en cuenta los siguientes datos para el environment que se les pedirá cuando se genere el contenedor.
```
# Directorio local de los proyectos
AGIP_WORKSPACE=~/workspace/services
# Directorio dentro de los contenedores
AGIP_DOCKER_WORKSPACE=/services
# Password del usuario root de mariadb
MYSQL_ROOT_PASSWORD=root
```

### Iniciar contenedores
Ejecutar desde el directorio donde clono `environment` 
```
./docker.sh up
```

### Listado de contenedores iniciados
```
./docker.sh status
```

### Detener contenedores
Ejecutar desde el directorio root del proyecto
```
./docker.sh stop
```

### Acceder a un contenedor
Ejecutar desde la consola
```
docker exec -it php7.3 bash
```
Para listar los contenedores ejecute `./docker.sh status`

### Crear un virtualhost para un nuevo proyecto
Nginx toma los archivos .conf que se encuentran en `$HOME/workspace/environment/docker/nginx/conf/`.

Puede tomar uno de los .conf existentes como ejemplo y modificarlo.
La propiedad `listen PORTNUMBER` debe indicarse en el archivo .conf y en el servicio enginx de `$HOME/workspace/environment/docker/docker-compose.yml`,
para luego acceder desde su ambiente al puerto indicado ej.: `http://localhost:83`.
Finalmente detenga e inicie los contenedores.

### Solucionar problemas con el servidor proxy

1. Editar `/etc/default/docker` y añadir/modificar los siguientes valores

```
DOCKER_OPTS="--dns 8.8.8.8 --dns 8.8.4.4"

export http_proxy=http://proxy.agip.gov.ar:3128
export https_proxy=http://proxy.agip.gov.ar:3128
export no_proxy=localhost,127.0.0.1
```

2. Editar/crear `/etc/docker/daemon.json`

```
{
    "dns": [
        "10.32.0.116",
        "8.8.8.8",
        "8.8.4.4"
    ]
}
```

3. Editar `/etc/resolv.conf` y añadir los siguientes valores

```
nameserver 127.0.0.53
nameserver 8.8.8.8
nameserver 8.8.4.4
```

4. Reiniciar el servicio

```
service docker restart
```

### Comandos útiles
```
docker-compose up [Lee archivo docker-compose.yml e incia los servicios configurados]
docker-compose up -d (Para que salga de la ejecución o sin el -d si queres ver el stout)
docker-compose build [Lee Archivo docker-compose.yml Y RECONFIGURA los servicios en caso de haber algún cambio].
docker ps [lista los contenedores iniciados]
docker ps -a [lista todos los contenedores]
docker stop 'nombre_or_id_container' [detiene un contenedor o varios]
docker stop $(docker ps -a -q) [detiene todos los contenedores que se están ejecutando]
docker start 'nombre_or_id_container' [inicia un contenedor o varios]
docker restart 'nombre_or_id_container' [reinicia un contenedor o varios]
docker rm 'nombre_or_id_container' [Elimina un contenedor]
docker rm $(docker ps -a -q) [Elimina todos los contenedores]
docker rmi 'nombre_or_id_imagen' [Elimina una imagen]
docker rmi $(docker images -q) [Elimina todas las imágenes]
docker exec -it 'nombre_or_id_container' bash [entras al contenedor]
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id" [Muestra IP de un contenedor]
ping nombre_or_id_container [comando dentro de algún contenedor para saber la ip de este].
docker-compose -f docker-compose.build.yml build
docker-compose -f docker-compose.build.yml up -d
Build images Tag an image (-t) docker build -t vieux/apache:2.0
docker run --name nginx -v /home/programacion/services:/usr/share/nginx/html:ro -d vmwarecna/nginx
docker save -o path/nombre_imagen.tar nombre_imagen [Guardar imagenes]
docker load -i path/nombre_imagen.tar [restaurar imagenes]
```