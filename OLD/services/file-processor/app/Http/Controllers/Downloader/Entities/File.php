<?php
namespace App\Http\Controllers\Downloader\Entities;

interface File
{
    public function __construct(string $fileUrl, string $fileName, string $storagePath);
    public function getLogin(String $user, String $pass) : void;
    public function getFile() : bool;
    public function processFile() : string;
}
