<?php
namespace App\Http\Controllers\Downloader;

use App\Http\Controllers\Downloader\Entities\File;
use App\Http\Controllers\Downloader\Entities\LocalFile;
use App\Jobs\ProcessRegisters;

class ConcreteDownloaderLocal extends AbstractDownloaderController
{
    const FILE_URL = 'http://ofiya.net/newsletter/';
    public $client = null;

    private $devLimit = 5;

    /**
     * Data with data for parsing
     * keyName => [offset, length, type (Default: String)]
     */
    const DATA_PARSER_LINES = [
        'first' => [0, 2, 'int'],
        'date' => [2, 10, 'date'],
        'cuil' => [13, 10, 'int'],
        'name' => [23, 80],
        'birthday' => [103, 11],
        'address' => [114, 30],
        'number' => [144, 11, 'int'],
        'apartment' => [155, 5],
        'comments' => [160, 37],
        'postal_code' => [198, 6, 'int'],
        'city' => [209, 30],
    ];


    public function __construct(String $fileName)
    {
        parent::__construct();

        $this->setFileName($fileName);
    }

    public function getResourceFile(?string $isLocalFile): string
    {
        $this->client = new LocalFile(self::FILE_URL, $this->getFileName(), $this->getStoragePath());
        if ($this->client->getFile()) {
            return $this->unzipFIle($this->getStoragePath(), $this->getFileName());
        }

        return null;
    }

    private function convertDataToArray(string $folderName): array
    {
        set_time_limit(0);

        $newData = [];

        $files = $this->readFilesIntoFolder($folderName);
        foreach ($files as $key => $value) {
            $filePath = $folderName . '/' . $value;
            $fn = fopen($filePath, "r");
            //$fn = file_get_contents($filePath);

            $i = 0;
            while (!feof($fn)) {
                $i++;
                $result = fgets($fn);
                $newData[] = $result;
                //echo $result;
                if (env('APP_ENV') != 'production') {
                    if ($i > $this->devLimit) {
                        break;
                    }
                }
            }
            /*while (($buffer = fgets($fn)) !== false) {
                if (trim($buffer) == "##########") {
                    break;
                }
                $newData .= $buffer;
                break;
            }*/
            //$newData = explode("\n", $fn);

            fclose($fn);
        }

        return $newData;
    }

    public function getProcessFile(string $folderName): string
    {
        $data = $this->convertDataToArray($folderName);
        $array = [];
        foreach ($data as $i => $line) {
            foreach (self::DATA_PARSER_LINES as $key => $limits) {
                $dataArray[$key] = trim(
                    preg_replace(
                        '!\s+!',
                        ' ',
                        substr(
                            $line,
                            $limits[0],
                            $limits[1]
                        )
                    )
                );

                if (!empty($limits[2])) {
                    switch ($limits[2]) {
                        case 'int':
                            $dataArray[$key] = (int)$dataArray[$key];
                            break;
                        case 'date':
                            $dataArray[$key] = strtotime($dataArray[$key]);
                            break;
                    }
                }
            }
            $array[$i] = $dataArray;
            ProcessRegisters::dispatch($dataArray);
            dd($dataArray);
        }
       // dd($array);
    }
}
