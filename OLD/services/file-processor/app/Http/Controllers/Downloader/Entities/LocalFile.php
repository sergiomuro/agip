<?php

namespace App\Http\Controllers\Downloader\Entities;

use Mockery\Exception;

class LocalFile implements File
{
    private $fileUrl = null;
    private $fileName = null;
    private $storagePath = null;

    public function __construct(string $fileUrl, string $fileName, string $storagePath)
    {
        $this->fileUrl = $fileUrl;
        $this->fileName = $fileName;
        $this->storagePath = $storagePath;
    }

    public function getFile(): bool
    {
        $url = $this->fileUrl . $this->fileName;
        $storagePathFile = $this->storagePath . '/' . $this->fileName;

        if (is_file($storagePathFile)) {
            return true;
        }

        try {
            $zipResource = fopen($storagePathFile, "w");

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FILE, $zipResource);
            $page = curl_exec($ch);
            if (!$page) {
                echo "Error :- " . curl_error($ch);
            }
            curl_close($ch);

        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    public function getLogin(String $user, String $pass): void
    {
        // TODO: Implement getLogin() method.
    }

    public function processFile(): string
    {
        // TODO: Implement getProcessFile() method.
    }
}
